/*******************************************************************************
1. DEPENDENCIES
*******************************************************************************/

var gulp = require('gulp'),
    del = require('del'),
    concat = require('gulp-concat'),
    stylus = require('gulp-stylus'),
    uglify = require('gulp-uglify'),
    coffee = require('gulp-coffee'),
    nib = require('nib'),
    jade = require('gulp-jade'),
    sourcemaps = require('gulp-sourcemaps'),
    pngcrush = require('imagemin-pngcrush'),
    browserSync = require('browser-sync'),
    prettify = require('gulp-html-prettify'),
    data = require('gulp-data'),
    fs = require('fs');


/*******************************************************************************
2. CLEAN TASK
*******************************************************************************/

gulp.task('clean', function (cb) {
    del(['release'], cb);
});

/*******************************************************************************
3. STYLUS TASK
*******************************************************************************/

gulp.task('stylesheets', function () {
    gulp.src(['dist/assets/stylesheets/common.styl'])
        .pipe(stylus({
            use: nib(),
            sourcemap: {
                inline: true,
                sourceRoot: '..',
                basePath: 'dist/assets/stylesheets'
            }
        }))
        .pipe(concat('main.css'))
        // .pipe(minifyCSS(opts))
        .pipe(gulp.dest('release/assets/stylesheets'));
});

/*******************************************************************************
4. SCRIPTS TASK
*******************************************************************************/

gulp.task('javascripts', function() {
    // Coffee compile
    gulp.src(['dist/assets/javascripts/coffee/*.coffee'])
        .pipe(coffee({bare: true}))
        .pipe(gulp.dest('release/assets/javascripts'));

    gulp.src(['dist/assets/javascripts/*.js'])
        .pipe(gulp.dest('release/assets/javascripts'));
});

/*******************************************************************************
5. TEMPLATES TASK
*******************************************************************************/

gulp.task('templates', function() {
    gulp.src(['dist/*.jade', 'dist/articles/*.jade', 'dist/galleries/*.jade'], {base: "dist"})
        .pipe(jade({
            pretty: true,
            data: (require('./content/stand1/stand_sections.json'))
        }))
        .pipe(gulp.dest('release'));

    gulp.src(['dist/templates/articles.jade'], {base: "dist/templates"})
        .pipe(jade({
            pretty: true,
            data: (require('./content/stand1/articles.json'))
        }))
        .pipe(gulp.dest('release'));

    gulp.src(['dist/templates/galleries.jade'], {base: "dist/templates"})
        .pipe(jade({
            pretty: true,
            data: (require('./content/stand1/galleries.json'))
        }))
        .pipe(gulp.dest('release'));
});


/*******************************************************************************
6. COPY ASSETS
*******************************************************************************/

gulp.task('copy', function() {
    gulp.src(['dist/assets/{fonts,images}/**'], {base: "dist"})
        .pipe(gulp.dest('release'));

    gulp.src(['dist/{public,vendor}/**'], {base: "dist"})
        .pipe(gulp.dest('release'));

    gulp.src(['bower_components/**'], {base: "."})
        .pipe(gulp.dest('release'));

    gulp.src(['content/stand1/**'], {base: "content/stand1"})
        .pipe(gulp.dest('release/public/uploads'))
});

/*******************************************************************************
7. BROWSER SYNC
*******************************************************************************/

gulp.task('browser-sync', function() {
    // http://www.browsersync.io/docs/options/
    // Options shown below is there ^
    browserSync({
        files: ["release/assets/stylesheets/*.css", "release/*.html", "release/assets/javascripts/*.js"],
        server: {
            baseDir: "release",
            index: "index.html",
            routes: {
                "/bower_components": "bower_components"
            }
        },
        codeSync: true,
        notify: false,
        // Do not open server in browser
        open: false
    });
});

/*******************************************************************************
8. START & WATCH
*******************************************************************************/

gulp.task('watch', function () {
    gulp.watch(['dist/assets/javascripts/coffee/*.coffee', 'dist/assets/javascripts/*.js'], ['javascripts']);
    gulp.watch('dist/assets/stylesheets/*.styl', ['stylesheets']);
    gulp.watch('dist/assets/images/**', ['copy']);
    gulp.watch('dist/**/*.jade', ['templates']);
});

gulp.task('default', ['stylesheets', 'javascripts', 'templates', 'copy', 'browser-sync', 'watch']);
// gulp.task('default', ['layoutize']);
