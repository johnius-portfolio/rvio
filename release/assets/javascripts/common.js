(function($) {
    $(function() {
        // window.catalog = {};

        // Preview swiper
        // function swiper1() {
        //     var mySwiper1 = new Swiper('.js__swiper_preview', {
        //         mode:'horizontal',
        //         slidesPerView: 'auto',
        //         // loop: true,
        //         preventLinksPropagation: true,
        //         scrollbar: {
        //             container :'.js__swiper_scrollbar1',
        //             draggable: false,
        //             hide: false
        //         }
        //     });
        //     $('.js__preview_prev').on('click', function(e){
        //         e.preventDefault()
        //         mySwiper1.swipePrev()
        //     });
        //     $('.js__preview_next').on('click', function(e){
        //         e.preventDefault()
        //         mySwiper1.swipeNext()
        //     });
        // };

        // Catalog
        // window.enableCatalogSwiper = function () {
        //     window.catalog.swiper = new Swiper('.js__swiper_catalog', {
        //         mode:'horizontal',
        //         slidesPerView: 'auto',
        //         preventLinksPropagation: true,
        //         // touchRatio: 0.3,
        //         freeMode: true,
        //         freeModeFluid: true,
        //         scrollbar: {
        //             container :'.js__swiper_scrollbar2',
        //             draggable: false,
        //             hide: false
        //         },
        //         onTouchMove: function (swiper) {
        //             swiper.container.setAttribute('data-omit-click', true)
        //         },
        //         onTouchEnd: function (swiper) {
        //             setTimeout(function () {
        //                 swiper.container.removeAttribute('data-omit-click')
        //             }, 750);
        //         },
        //     });
        // };

        // window.toggleHeader = function (cb) {
        //     var $header = $('#header'),
        //         toggled = $header.get(0).getAttribute('data-shown'),
        //         top;

        //     if (toggled) {
        //         top = '-120px';
        //         $header.removeAttr('data-shown');
        //     }
        //     else {
        //         $header.attr('data-shown', true);
        //         top = 0;
        //     }

        //     $header.animate({
        //         top: top
        //     }, 900, "easeOutBounce", function() {
        //         if (cb)
        //             cb();
        //     });
        // }

        // window.toggleFooter = function (cb) {
        //     var $header = $('#footer'),
        //         toggled = $header.get(0).getAttribute('data-shown'),
        //         bottom;

        //     if (toggled) {
        //         bottom = '-85px';
        //         $header.removeAttr('data-shown');
        //     }
        //     else {
        //         $header.attr('data-shown', true);
        //         bottom = 0;
        //     }

        //     $header.animate({
        //         bottom: bottom
        //     }, 900, "easeOutBounce", function() {
        //         if (cb)
        //             cb();
        //     });
        // }

        // window.toggleToolbar = function () {
        //     toggleHeader();
        //     toggleFooter();
        // }

        // Article style change
        $('[data-theme]').on('click', function() {
            $('[id^="Article"]').toggleClass('article__dark')
            return false;
        });

        // Text zooming in article
        $('[data-plus]').on('click', function() {
            $('[id^="Article"]').each(function () {
                var par = $(this);
                var current;

                par.find('[data-text-size] li').each(function(index) {
                    if($(this).hasClass('current')) {
                        current = index-1;
                    }
                });

                if(current>=0) {
                    par.find('[data-text-size] li.current').removeClass('current').prev().addClass('current');
                    if(current==0) {
                        par.addClass('article__big');
                    } else if(current==1) {
                        par.removeClass('article__small');
                    }
                }

                $(this).find('.article').get(0).scrollTop = 0;
            });

            return false;
        });
        $('[data-minus]').on('click', function() {
            $('[id^="Article"]').each(function () {
                var par = $(this);
                var current;
                par.find('[data-text-size] li').each(function(index) {
                    if($(this).hasClass('current')) {
                        current = index+1;
                    }
                });
                if(current<3) {
                    par.find('[data-text-size] li.current').removeClass('current').next().addClass('current');
                    if(current==2) {
                        par.addClass('article__small');
                    } else if(current==1) {
                        par.removeClass('article__big');
                    }
                }


                $(this).find('.article').get(0).scrollTop = 0;
            });

            return false;
        });

        // $('.organizers__inner').kinetic({x: false});

        // Organizers
        // function swiper4() {
        //     var mySwiper4 = new Swiper('.js__swiper_organizers', {
        //         scrollContainer:true,
        //         mousewheelControl : true,
        //         mode:'vertical',
        //         scrollbar: {
        //             container :'.js__swiper_scrollbar4',
        //             draggable: false,
        //             hide: false
        //         }
        //     });
        // };


        // function scrollEvent() {
        //     $('.js__article div').trigger('scroll');
        //     $('.js__article div').scroll();
        //     $('.js__article .swiper-wrapper').css({
        //         transform: 'translate3d(0px, 0px, 0px)'
        //     })
        // };


        // // Article
        // var mySwiper3 = new Swiper('.js__swipe_article', {
        //     scrollContainer:true,
        //     mousewheelControl : true,
        //     mode:'vertical',
        //     scrollbar: {
        //         container :'.js__swiper_scrollbar3',
        //         draggable: false,
        //         hide: false
        //     },
        //     onInit: function() {
        //         scrollEvent();
        //     }
        // });

        // $('.js__enter').on('click', function() {
        //     $(this).fadeOut(900, function() {
        //         toggleIntroVideo.call(this);
        //         toggleHeader(function() {
        //             $('.js__attention').fadeIn(900);
        //         });
        //         toggleFooter();
        //     });
        //     return false;
        // });

        // $('.js__attention_link').on('click', function() {
        //     $(this).parents('.js__attention').fadeOut(900, function() {
        //         $('.footer__nav_hidden').css({
        //             'display': 'block'
        //         });
        //         $('.footer__nav_catalog').addClass('current');
        //         $('.js__catalog').fadeIn(500);
        //         $('.blocked').removeClass('blocked');
        //         // mySwiper2
        //         swiper2();
        //     });
        //     return false;
        // });

        // $('.js__close').on('click', function() {
        //     $(this).parents('.js__article').fadeOut(900, function() {
        //         $('.js__preview').fadeIn(900);

        //     });
        //     return false;
        // });

        // Open preview
        // $('.catalog__block').on('click', function() {
        //     var click_omitted = $(this).closest('[data-omit-click]').length;
        //     if (!click_omitted) {
        //         $('.footer__nav_catalog').removeClass('current');
        //         $(this).parents('.js__catalog').fadeOut(900, function() {
        //             $('.js__preview').fadeIn(900, function() {
        //                 // mySwiper1
        //                 swiper1();
        //             });

        //         });
        //     }
        //     return false;
        // });

        // Preview
        // $('body').on('click', '.js__article_link', function(e) {
        //     e.stopPropagation();
        //     e.preventDefault();
        //     var href = $(this).attr('href');
        //     $(this).parents('.js__preview').fadeOut(900, function() {
        //         $('.js__article').fadeIn(900);
        //         $('.article').load(href, function() {
        //             // mySwiper3
        //             mySwiper3.reInit();
        //         });

        //     });
        // });
        // $('.js__preview_close').on('click', function() {
        //     $('.footer__nav_catalog').addClass('current');
        //     $(this).parents('.js__preview').fadeOut(900, function() {
        //         $('.js__catalog').fadeIn(900);

        //     });
        //     return false;
        // });

        // Back
        // $('.js__back_link').on('click', function() {
        //     if(!$(this).hasClass('current')&&!$(this).hasClass('blocked')) {
        //         $('.footer__nav_catalog').addClass('current');
        //         $('.js__preview, .js__article').fadeOut(900);
        //         setTimeout(function() {
        //             $('.js__catalog').fadeIn(900);

        //         }, 900);
        //     }
        //     return false;
        // });

        // Help
        // $('.js__help_link').on('click', function() {
        //     $('div[data-help]').each(function() {
        //         if($(this).css('display') == 'block') {
        //             var val = $(this).data('help');
        //             $('.js__help_block').attr({src: val}).show();
        //         }
        //     });
        //     return false;
        // });
        // $('.js__help_block').on('click', function() {
        //     $(this).hide();
        //     return false;
        // });

        // // Organizers
        // $('.js__organizers__link').on('click', function() {
        //     if(!$(this).hasClass('blocked')) {
        //         $('#content').fadeOut(900, function() {
        //             $('.js__organizers').fadeIn(900);
        //             // swiper4();
        //         });
        //     }
        //     return false;
        // });
        // $('.js__organizers_close').on('click', function() {
        //     $('.js__organizers').fadeOut(900, function() {
        //         $('#content').fadeIn(900);
        //     });
        //     return false;
        // });


    });
})(jQuery);
