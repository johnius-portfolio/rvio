$ ->
  log = ->
    console.log.apply console, arguments

  $('document').on 'click', 'a', (e) ->
    e.preventDefault()

  $('*:not(.swiper-container)').on 'dragstart drop', ->
    false

  # Enter
  window.current = document.getElementById('Enter')

  window.disableEnterVideo = ->
    unless window.intro_video
      window.intro_video = $('#Enter video').remove()

  window.enableEnterVideo = ->
    if window.intro_video
      $('#Enter').prepend(window.intro_video).find('video').get(0).play()
      delete(window.intro_video)

  window.onHideEnter = ->
    $('#Footer')
      .queue ->
        $(this).css
          opacity: 1
          bottom: -85
        $(this).show()
        $(this).dequeue()
      .animate {bottom: 0},
        duration: 900
        easing: 'easeOutBounce'
    $('#Header')
      .queue ->
        $(this).css
          opacity: 1
          top: -125
        $(this).show()
        $(this).dequeue()
      .animate { top: 0 },
        duration: 900
        easing: 'easeOutBounce'
    disableEnterVideo()

  window.onShowEnter = ->
    window.history_ary = []
    $('#Footer')
      .queue ->
        $(this).css('opacity', 1)
        $(this).dequeue()
      .fadeOut(450)
    $('#Header')
      .queue ->
        $(this).css('opacity', 1)
        $(this).dequeue()
      .fadeOut(450)
    enableEnterVideo()

  window.returnBackToEnterIn3MinutesStay = ->
    clearTimeout(window.returnBackToEnterIn3MinutesStayTimer)
    window.returnBackToEnterIn3MinutesStayTimer = setTimeout ->
      hideThisShowThis(window.current, $('#Enter'))
    , 180000


  # Catalog
  window.onShowCatalog = ->
    window.catalog_swiper = new Swiper '#Catalog .swiper-container',
      mode: 'horizontal'
      slidesPerView: 'auto'
      preventLinksPropagation: true
      freeMode: true
      freeModeFluid: true
      scrollbar:
        container: '.swiper-scrollbar'
        draggable: false
        hide: false
      onTouchMove: (swiper) ->
        swiper.container.setAttribute('data-omit-click', true)
      onTouchEnd: (swiper) ->
        setTimeout ->
          swiper.container.removeAttribute('data-omit-click')
        , 750

  window.onHideCatalog = ->
    window.catalog_swiper.destroy()


  # Organizers
  window.onShowOrganizers = ->
    $('#Organizers [data-kinetic]').get(0).scrollTop = 0
    $('#Organizers [data-kinetic]').kinetic('attach', {x: false})

  window.onHideOrganizers = ->
    $('#Organizers [data-kinetic]').kinetic('detach')


  # Preview
  window.onShowPreview = (el, caller) ->
    if (caller)
      initial_slide = caller.getAttribute('data-id')
    else
      initial_slide = 0

    window.preview_swiper = new Swiper '#Preview .swiper-container',
      mode: 'horizontal'
      slidesPerView: '1'
      initialSlide: initial_slide
      onTouchMove: (swiper) ->
        swiper.container.setAttribute('data-omit-click', true)
      onTouchEnd: (swiper) ->
        setTimeout ->
          swiper.container.removeAttribute('data-omit-click')
        , 750
      scrollbar:
        container: '.swiper-scrollbar'
        draggable: false
        hide: false

    $('#Preview .preview__prev').on 'click', (e) ->
      e.preventDefault()
      preview_swiper.swipePrev()

    $('#Preview .preview__next').on 'click', (e) ->
      e.preventDefault()
      preview_swiper.swipeNext()

  window.onHidePreview = ->
    window.preview_swiper.destroy()


  # Gallery
  window.onShowGallery = (el, caller) ->
    if (caller)
      initial_slide = caller.getAttribute('data-photo-id')
      gallery_id = caller.getAttribute('data-id')
    else
      initial_slide = 1

    window.gallery_swiper = new Swiper '#Gallery' + gallery_id + ' .swiper-container',
      mode: 'horizontal'
      slidesPerView: 'auto'
      centeredSlides: true
      initialSlide: initial_slide
      onSwiperCreated: (swiper) ->
        $(swiper.container).find('.kinetic').each ->
          $(this).kinetic('attach', {x: false})

  window.onHideGallery = ->
    $(window.gallery_swiper.container).find('.kinetic').each ->
      $(this).get(0).scrollTop = 0
      $(this).kinetic('detach')
    window.gallery_swiper.destroy()
    $(window.gallery_swiper.container).find('[id^="Gallery"]').hide()
    delete(window.gallery_swiper)


  # Article
  window.onShowArticle = (el, caller) ->
    if (caller)
      article_id = caller.getAttribute('data-id')

      $('#Article'+article_id+' .article').get(0).scrollTop = 0
      $('#Article'+article_id+' .article').kinetic('attach', {x: false, triggerHardware: true, throttleFPS: 30})

  window.onHideArticle = (el, caller) ->
    if (caller)
      article_id = caller.getAttribute('data-id')

    $('#Article'+article_id+' .article').kinetic('detach')

  # API
  window.history_ary ?= []
  goBack = (current) ->
    $current = $(current)
    if (previous = window.history_ary.pop())
      $previous = $('#' + previous)
      hideThisShowThis($current, $previous, true)
    else
      console.log 'Nothing to go'

  addLatest = (id) ->
    history = window.history_ary
    history.push(id)

  getLatest = ->
    if (length = window.history_ary.length)
      window.history_ary[length - 1]
    else
      console.log 'getLatest :: no history there'
      false

  onHide = (el) ->
    glob = el.getAttribute('data-onhide')
    if window.hasOwnProperty(glob)
      window[glob](el)

  onShow = (el, caller) ->
    glob = el.getAttribute('data-onshow')
    if window.hasOwnProperty(glob)
      window[glob](el, caller)
    returnBackToEnterIn3MinutesStay()

  $(document).on 'click', '[data-close]', ->
    unless window.animating
      goBack.call(null, window.current)

  hideThisShowThis = (hide, show, omit_history_push, caller) ->
    $next = $(show)
    $current = $(hide)

    if $next.is $current
      return

    window.animating = true
    $current.fadeOut 500, ->
      $(this).removeAttr('data-current')
      window.previous = this

      unless omit_history_push
        console.log('Adding latest to:', this.id)
        addLatest(this.id)
      onHide(this)

      $next.queue ->
        window.current = this
        $(this).css('opacity', 0)
        $(this).show()
        onShow(this, caller)
        $(this).attr('data-current', true)
        $(this).dequeue()
      .animate
        opacity: 1,
        500,
        ->
          window.animating = false


  $(document).on 'click', '[data-show]', ->
    if !window.animating && !$(this).closest('[data-omit-click]').length
      $next = $(document.getElementById(this.getAttribute('data-show')))
      $current = $('#' + window.current.id)
      args = arguments

      hideThisShowThis($current, $next, false, this)


  # preview_swiper = new Swiper '[data-preview-swiper]',
  #   mode: 'horizontal'
  #   slidesPerView: 'auto'
  #   preventLinksPropagation: true
  #   scrollbar:
  #     container: '[data-preview-swiper] .swiper-scrollbar'
  #     draggable: false
  #     hide: false

  # $('[data-preview-prev]').on 'click', ->
  #   preview_swiper.swipePrev()

  # $('[data-preview-next]').on 'click', ->
  #   preview_swiper.swipeNext()

  # window.unloadPrevious = ->
  #   if window.hasOwnProperty('onPageUnload') && window.loaded
  #     window.onPageUnload()